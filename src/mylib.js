/** Basic arithmetic operations */
const mylib = {
    /** Multiline arrow function. */
    add: (a, b) => {
        const sum = a + b;
        return sum;
    },
    subtract: (a, b) => {
        return a - b;
    },
    /** Singleline arrow function. */
    divide: (dividend, divisor) => {
        if (divisor == 0){
            const error = new Error('ZeroDivision');
            return error.message;
        } else {
            return dividend / divisor;
        }
        
    },
    /** Regular function. */
    multiply: function(a, b) {
        return a * b;
    }
};

module.exports = mylib;