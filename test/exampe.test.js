const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Our first unit tests", () => {
    before(() => {
        //initalization
        // create objects... etc...
        console.log("Initialising tests.");
    });
    it("Can add 1 and 2 together", () => {
        // Test add
        expect(mylib.add(1, 2)).equal(3, "1 + 2 is not 3, for some reason?");
    });
    it("Can substrack 11 from 2", () => {
        // Test subsrack
        expect(mylib.subtract(11, 2)).equal(9, "11 - 2 not 9, for some reason?");
    });
    it("Can multiply 9 by 2", () => {
        // Test multyply
        expect(mylib.multiply(9, 2)).equal(18, "9 * 2 not 18, for some reason?");
    });
    it("Can divide 20 by 10", () => {
        // Test divide
        expect(mylib.divide(20, 10)).equal(2, "20 / 10 not 2, for some reason?");
    });
    it("Can divide 20 by 0", () => {
        // Test divide
        expect(mylib.divide(20, 0)).equal('ZeroDivision', "20 / 0 not ZeroDivision, for some reason?");
    });
    after(() => {
        // Cleanup
        // For example: shautdown the Express server.
        console.log("Testing completed!");
    });
});